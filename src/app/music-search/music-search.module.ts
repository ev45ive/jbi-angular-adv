import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchViewComponent } from './containers/music-search-view/music-search-view.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SyncSearchComponent } from './containers/sync-search/sync-search.component';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';


@NgModule({
  declarations: [
    MusicSearchViewComponent, 
    SearchFormComponent, 
    SearchResultsComponent, 
    AlbumCardComponent, SyncSearchComponent, AlbumDetailsComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MusicSearchRoutingModule
  ]
})
export class MusicSearchModule { }
