import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { MusicSearchViewComponent } from './containers/music-search-view/music-search-view.component';
import { SyncSearchComponent } from './containers/sync-search/sync-search.component';


const routes: Routes = [
  {
    path: 'search',
    children: [
      {
        path: '',
        component: MusicSearchViewComponent
      },
      {
        path: 'sync',
        component: SyncSearchComponent
      },
      {
        path: 'albums/:album_id',
        component: AlbumDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
