import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators, Validator, AsyncValidatorFn } from '@angular/forms';
import { combineLatest, Observable, pipe } from 'rxjs';
import { debounceTime, distinct, distinctUntilChanged, filter, map, tap, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  censor = (badword: string): ValidatorFn => //
    (control: AbstractControl): ValidationErrors | null => {
      const isInvalid = String(control.value).includes(badword)

      return isInvalid ? { 'censor': { badword } } : null
    }

  asyncCensor = (badword: string): AsyncValidatorFn => (control: AbstractControl) => {
    // return this.http.post('/validate',{value}).pipe(map(res=>res.errors | null))

    return new Observable((observer) => {
      const isInvalid = String(control.value).includes(badword)
      // console.log('Subscribed')

      const handler = setTimeout(() => {
        const result = isInvalid ? { 'censor': { badword } } : null
        // console.log('Next')

        observer.next(result)
        observer.complete()
      }, 500)

      return () => {
        clearTimeout(handler);
        // console.log('Unsubscribed')
      }
    })
    // .subscribe({
    //   next: val => console.log(val)
    // })
  }

  @Input()
  set query(query:string){
    (this.searchForm.get('query') as FormControl)?.setValue(query,{
      emitEvent:false, 
      // onlySelf:true // do not notify FormGroup
    })
  }

  searchForm = new FormGroup({
    query: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      // this.censor('batman')
    ], [
      this.asyncCensor('batman')
    ]),
  })

  constructor(private fb: FormBuilder) {
    (window as any).form = this.searchForm

    const queryField = this.searchForm.get('query');
    const statusChanges = queryField!.statusChanges
    const valueChanges = queryField!.valueChanges

    // combineLatest([valueChanges,statusChanges]).pipe(

    const onlyIfValid = (valueChanges: Observable<string>) => pipe(
      // tap(console.log),
      debounceTime(400),
      withLatestFrom(valueChanges),
      filter(([status, value]) => status == 'VALID'),
      map(([status, value]) => value)
    )

    statusChanges.pipe(
      onlyIfValid(valueChanges)
    ).subscribe(this.searchChange)

    this.searchButtonClicked.pipe(
      withLatestFrom(statusChanges),
      map(([click, status]) => status),
      onlyIfValid(valueChanges)
    ).subscribe(this.searchChange)
  }

  ngOnInit(): void {
  }

  @Output() searchChange = new EventEmitter<string>();

  private searchButtonClicked = new EventEmitter()

  search() {
    // console.log(this.query)
    this.searchButtonClicked.emit()
    // this.searchChange.emit(this.searchForm.get('query')?.value)
  }
}
