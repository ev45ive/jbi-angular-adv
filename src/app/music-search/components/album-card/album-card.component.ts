import { ChangeDetectionStrategy, Component, HostBinding, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Album } from 'src/app/core/model/search';
import { SearchResultsComponent } from '../search-results/search-results.component';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss'],
  // host:{
  //   '[class.card]':' isClass ',
  //   '[style.border]':' "1px solid black" ',
  //   '(click)':'iwascliked($event)'
  // }
  changeDetection:ChangeDetectionStrategy.OnPush
})  
export class AlbumCardComponent implements OnInit, OnChanges {

  @Input()
  album!: Album

  @HostBinding('class.card')
  isClass = true

  @Input()
  @HostBinding('style.min-width.%')
  @HostBinding('style.max-width.%')
  width = 25

  constructor(
    // private results:SearchResultsComponent
  ) { 
    // debugger
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['album'].isFirstChange && !this.album) {
      throw 'AlbumCardComponent requires [album] of type Album'
    }
  }

  ngOnInit(): void {
  }

}
