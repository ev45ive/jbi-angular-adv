import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, pluck, switchMap, tap } from 'rxjs/operators';
import { Track } from 'src/app/core/model/search';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  album_id = this.route.params.pipe(
    pluck('album_id')
  )

  album = this.album_id.pipe(
    switchMap(id => this.service.getAlbumById(id)),
    tap(() => this.setupPlayer())
  )

  selectedTrack?: Track

  @ViewChild('audioRef', {
    read: ElementRef,
    static: false
  })
  audioRef?: ElementRef<HTMLAudioElement>

  setupPlayer() {
    if (this.audioRef) {
      this.audioRef.nativeElement.volume = 0.2
    }
  }

  selectTrack(track: Track) {
    this.selectedTrack = track
    setTimeout(() => {
      this.audioRef?.nativeElement.play()
    })
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {

  }

  ngOnInit(): void {
  }

}
