import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectableObservable, ReplaySubject, Subject, Subscription } from 'rxjs';
import { map, multicast, publish, publishReplay, refCount, share, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { Album } from 'src/app/core/model/search';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-music-search-view',
  templateUrl: './music-search-view.component.html',
  styleUrls: ['./music-search-view.component.scss']
})
export class MusicSearchViewComponent implements OnInit {
  message = ''
  results = this.service.getAlbums()
  query = this.service.queryChanges

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService) {

    // const query = this.route.snapshot.queryParamMap.get('q')
    // if (query) {
    //   this.search(query)
    // }
  }

  ngOnInit(): void {
    // this.route.snapshot.params['q']

    this.route.queryParamMap.subscribe(paramMap => {
      const query = paramMap.get('q')
      if (query) {
        // this.search(query)
        this.service.searchAlbums(query)
      }
    })

  }

  search(query: string) {

    // this.router.navigate(['/search'],{
    this.router.navigate([], {
      relativeTo: this.route,
      replaceUrl: true,
      queryParams: {
        q: query
      }
    })
  }


}

