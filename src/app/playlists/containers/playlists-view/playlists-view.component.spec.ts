import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { mockplaylists, PlaylistsService } from 'src/app/core/services/playlists/playlists.service';
import { routes } from '../../playlists-routing.module';

import { PlaylistsViewComponent } from './playlists-view.component';

@Component({
  template: `<router-outlet></router-outlet>`
})
export class MockRouterParent { }

fdescribe('PlaylistsViewComponent', () => {
  let router: Router;
  // let location: Location;
  let component: PlaylistsViewComponent;
  let fixture: ComponentFixture<PlaylistsViewComponent>;
  let parentfixture: ComponentFixture<MockRouterParent>;
  let service: PlaylistsService

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes, {})
      ],
      providers: [
        {
          provide: PlaylistsService,
          useFactory() {
            return jasmine.createSpyObj<PlaylistsService>(
              'PlaylistsService', ['getCurrentUserPlaylists', 'getPlaylistById'])
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [PlaylistsViewComponent, MockRouterParent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    parentfixture = TestBed.createComponent(MockRouterParent);
    router = TestBed.inject(Router)
    service = TestBed.inject(PlaylistsService)
    router.initialNavigation()
    // location = TestBed.inject(Location)
    parentfixture.detectChanges();
  });

  it('should fetch all', fakeAsync(() => {
    (<jasmine.Spy>service.getCurrentUserPlaylists).and
    .returnValue(of(mockplaylists))
    router.navigate(['/playlists'])
    tick();
    // expect(location.path()).toEqual('/playlists')
    parentfixture.detectChanges();
    tick();
    // debugger
    
    expect(service.getCurrentUserPlaylists).toHaveBeenCalled()

    component = parentfixture.debugElement.query(By.directive(PlaylistsViewComponent)).componentInstance
    expect(component).toBeTruthy();
  }))


  it('should fetch selected',  fakeAsync(() => {
    (<jasmine.Spy>service.getPlaylistById).and
    .returnValue(of(mockplaylists[0]))
    router.navigate(['/playlists/123'])
    tick();
    // expect(location.path()).toEqual('/playlists')
    parentfixture.detectChanges();
    tick();
    // debugger

    expect(service.getPlaylistById).toHaveBeenCalledWith('123')
  }))
});
