import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Playlist } from 'src/app/core/model/playlist';
import { mockplaylists, PlaylistsService } from 'src/app/core/services/playlists/playlists.service';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode: 'details' | 'form' = 'details'

  playlists = this.service.getCurrentUserPlaylists()

  selectedPlaylist = this.route.paramMap.pipe(
    map(paramMap => (paramMap.get('playlist_id'))),
    switchMap((playlist_id) =>
      playlist_id ? this.service.getPlaylistById(playlist_id) : of(null)
    )
  )

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {  }

  ngOnInit(): void {
  }

  selectPlaylist(playlist: Playlist) {
    this.router.navigate(['/playlists', playlist.id])

  }

  edit() {
    this.mode = 'form'
  }


  cancel() {
    this.mode = 'details'

  }

  save(draft: Playlist) {
    // const index = this.playlists.findIndex(p => p.id == draft.id)
    // if (index !== -1) {
    //   this.playlists.splice(index, 1, draft)
    // }
    // this.selectedPlaylist = draft;

    // this.mode = 'details'
  }
}
