import { EventEmitter, Input, Output, OnChanges, DoCheck, OnDestroy, SimpleChanges, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit, OnChanges, DoCheck, OnDestroy, AfterViewInit {

  @Input()
  playlist!: Playlist

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();

  // @ViewChild('formRef', { static: true, read:NgForm })
  // @ViewChild('formRef', { static: true, read:ElementRef })
  @ViewChild(NgForm, { static: true, read: NgForm })
  formRef?: NgForm

  cancelClicked() {
    this.cancel.emit()
  }

  saveClicked(formRef: NgForm) {
    // formRef.form.disable()
    // x.setValue({name: "Playlist ", public: false, description: "test "})
    // x.form.patchValue({name:'Partia;', zxc:123})

    const { name, info: {
      public: isPublic, description
    } } = formRef.value

    const draft: Playlist = {
      ...this.playlist, name,
      public: isPublic,
      description
    }
    this.save.emit(draft)
  }

  constructor() {
    console.log('constructor')
  }

  ngAfterViewInit(): void {
    setTimeout(()=>{
      console.log(this.formRef?.value)
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes)
  }

  ngOnInit(): void {
    console.log('ngOnInit')
  }

  ngDoCheck(): void {
    console.log('ngDoCheck')
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy')
  }
}
