import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist!: Playlist

  // ng-output
  @Output() edit = new EventEmitter();

  editClicked() {
    this.edit.emit()
  }

  constructor() { }

  ngOnInit(): void {
  }

}
