
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { mockplaylists } from 'src/app/core/services/playlists/playlists.service';
import { YesnoPipe } from 'src/app/shared/yesno.pipe';

import { PlaylistDetailsComponent } from './playlist-details.component';

describe('PlaylistDetailsComponent', () => {
  let component: PlaylistDetailsComponent;
  let fixture: ComponentFixture<PlaylistDetailsComponent>;
  let debugElement: DebugElement;

  beforeEach(async (/* done */) => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistDetailsComponent, YesnoPipe],
      schemas: [/* NO_ERRORS_SCHEMA, */ CUSTOM_ELEMENTS_SCHEMA]
    })
      .overridePipe(YesnoPipe, {  })
      .compileComponents() //.then(done)
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsComponent);
    debugElement = fixture.debugElement
    component = fixture.componentInstance;
    component.playlist = mockplaylists[0]

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('renders playlist details', () => {
    const name = debugElement.query(By.css('[data-test-id=playlist-name]'))
    
    expect(name.nativeElement.textContent).toEqual(mockplaylists[0].name)
  })
  
  it('should change style of public',()=>{
    const publicEl = debugElement.query(By.css('[data-test-id="playlist_public"]'))

    expect(publicEl.nativeElement).toHaveClass('is-private')
        
    component.playlist = mockplaylists[1]
    fixture.detectChanges();
    
    expect(publicEl.nativeElement).toHaveClass('is-public')
  })
});

// https://github.com/ngneat/spectator
// Custom Jasmine Matchers
// https://medium.com/bb-tutorials-and-thoughts/angular-how-to-add-jasmine-custom-matchers-in-unit-testing-3edd40f567ec

