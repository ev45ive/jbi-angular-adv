import { NgForOf, NgForOfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  // encapsulation:ViewEncapsulation.None
  // encapsulation:ViewEncapsulation.ShadowDom,
  // inputs:['playlists:items'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class PlaylistListComponent implements OnInit {

  @Input('items')
  playlists: Playlist[] = []

  @Input()
  selected?: Playlist

  @Output()
  selectedChange = new EventEmitter<Playlist>() 

  constructor() { }

  trackById(index: number, item: Playlist) {
    return item.id
  }

  select(playlist: Playlist) {
    // this.selectedChange.subscribe(console.log)
    // this.selected = playlist
    this.selectedChange.emit(playlist)
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    
  }

}
