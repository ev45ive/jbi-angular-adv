import { ElementRef, TemplateRef, ViewContainerRef } from '@angular/core';
import { UnlessDirective } from './unless.directive';

describe('UnlessDirective', () => {
  let directive: UnlessDirective
  let vcr: ViewContainerRef

  beforeEach(() => {
    vcr = jasmine.createSpyObj('ViewContainerRef', ['clear', 'createEmbeddedView'])

    directive = new UnlessDirective(
      {} as ElementRef,
      {} as TemplateRef<any>,
      vcr as ViewContainerRef,
    );
  })


  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('creates view from given template', () => {

    directive.hide = (true)
    expect(vcr.clear).toHaveBeenCalled()
  })
});
