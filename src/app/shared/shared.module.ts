import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YesnoPipe } from './yesno.pipe';
import { CardComponent } from './card/card.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabpanelComponent } from './tabpanel/tabpanel.component';
import { UnlessDirective } from './unless.directive';



@NgModule({
  declarations: [YesnoPipe, CardComponent, TabsComponent, TabpanelComponent, UnlessDirective],
  imports: [
    CommonModule
  ],
  exports: [YesnoPipe, CardComponent, TabsComponent, TabpanelComponent, UnlessDirective]
})
export class SharedModule { }
