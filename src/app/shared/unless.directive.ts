import { Directive, ElementRef, HostBinding, Input, TemplateRef, ViewContainerRef } from '@angular/core';

interface appUnlessContext {
  $implicit: any
}

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
  // @HostBinding('hidden')
  // hidden = false;
  
  @Input('appUnless')
  set hide(hidden: boolean) {
    
    if(hidden){
      this.vcr.clear()
    }else{
      const view = this.vcr.createEmbeddedView(this.tpl,{
        $implicit:'Pancakes'
      },0)
    }
  }

  constructor(
    private elem: ElementRef,
    private tpl: TemplateRef<appUnlessContext>,
    private vcr: ViewContainerRef
  ) {
    // console.log('HEllo unlesss')  
    // $(this.elem.nativeElement).yourFavJqueryPlugin()
  }


  ngOnDestroy() {
    // $(this.elem.nativeElement).yourFavJqueryPlugin().destroy()
  }
}
