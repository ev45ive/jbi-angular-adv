import { Attribute, Component, HostBinding, Input, OnInit } from '@angular/core';
import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tabpanel',
  templateUrl: './tabpanel.component.html',
  styleUrls: ['./tabpanel.component.scss']
})
export class TabpanelComponent implements OnInit {

  @HostBinding('class.card')
  isCard = true

  isOpen = false

  toggle() {
    // this.isOpen = !this.isOpen
    this.tabsComp.toggle(this)
  }

  @Input()
  label = ''

  constructor(/* @Attribute() label */
    private tabsComp: TabsComponent
  ) {
    this.tabsComp.tabs.push(this)
  }

  ngOnInit(): void {
  }

  ngOnDestory() {
    const index = this.tabsComp.tabs.indexOf(this)
    if (index == -1) {
      this.tabsComp.tabs.splice(index, 1)
    }
  }

}
