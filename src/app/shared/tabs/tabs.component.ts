import { Component, OnInit } from '@angular/core';
import { TabpanelComponent } from '../tabpanel/tabpanel.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  tabs: TabpanelComponent[] = [];

  toggle(tab:TabpanelComponent){
    this.tabs.forEach(currentTab => {
      currentTab.isOpen = tab == currentTab
    })
  }

  constructor() { }

  ngOnInit(): void {
  }

}
