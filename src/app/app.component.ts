import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-root, app-pancakes',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

  expanded = false
  title = 'Angular-adv';
  hide = false;

  user = {
    loggedIn: false,
    name: 'User',
    getName() {
      return this.loggedIn ? this.name : 'Anonymous'
    }
  }

}

// console.log(AppComponent)