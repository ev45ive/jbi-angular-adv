import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private auth: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const authRequest = request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    })
    
    return next.handle(authRequest).pipe(
      catchError((err, caughtObservable) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status == 401) { this.auth.authorize() }
          return throwError(new Error(err.error.error.message))
        }
        return throwError(new Error('Unexpected error!'))
      })
    )
  }
}

// Observable<res> = A.handle(req)
// A.next = B
// B.next = C
// C.next = Server.handle(req)