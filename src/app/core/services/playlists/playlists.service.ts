import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Playlist } from '../../model/playlist';
import { PagingObject } from '../../model/search';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {
  api_url = 'https://api.spotify.com/v1'

  getCurrentUserPlaylists() {
    return this.http.get<PagingObject<Playlist>>(this.api_url + '/me/playlists').pipe(
      map(resp => resp.items)
    )
  }

  getPlaylistById(playlist_id:string){
    return this.http.get<Playlist>(this.api_url+`/playlists/${playlist_id}`)
  }

  constructor(private http: HttpClient) { }
}

export const mockplaylists: Playlist[] = [
  { type: 'Playlist', id: '123', name: 'Playlist 123', public: false, description: 'test 123' },
  { type: 'Playlist', id: '234', name: 'Playlist 234', public: true, description: 'test 234' },
  { type: 'Playlist', id: '345', name: 'Playlist 345', public: false, description: 'test 345' },
]
