import { TestBed } from '@angular/core/testing';
import { PlaylistsService } from './playlists.service';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('PlaylistsService', () => {
  let service: PlaylistsService;
  let controller: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [HttpClientTestingModule],
      providers: [],
    });
    service = TestBed.inject(PlaylistsService);
    controller = TestBed.inject(HttpTestingController)
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('shoud fetch current user playlists', () => {
    const spy = jasmine.createSpy('ResponseSpy')

    service.getCurrentUserPlaylists().subscribe({
      next: spy
    })

    const req = controller.expectOne('https://api.spotify.com/v1/me/playlists', 'MyPlaylsits')

    const mockPlaylists = [{ id: 123, name: 'Fake Playlist' }];
    // req.request.
    req.flush({
      items: mockPlaylists
    })

    expect(spy).toHaveBeenCalledWith(mockPlaylists)

    controller.verify()
  })
});
