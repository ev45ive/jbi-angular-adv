import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root',
  useValue: environment.auth_config
})
export abstract class AuthConfig {
  /**
   * endpoint to redirect user to
   */
  auth_endpoint = '';
  client_id = '';
  response_type = '';
  redirect_uri = '';
  state = '';
  scopes: string[] = [];
  show_dialog = false;
}

