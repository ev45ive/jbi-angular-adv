import { HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AuthConfig } from './AuthConfig';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string | null = null;

  constructor(private config: AuthConfig) { }

  init() {
    if (!this.token) {
      this.extractToken()
    }
    if (!this.token) {
      this.authorize()
    }
  }

  authorize() {
    const { auth_endpoint, client_id, redirect_uri, response_type, scopes, show_dialog, state } = this.config

    let p = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type,
        scope: scopes.join(' '),
        state
      }
    })
    if (show_dialog) {
      p = p.set('show_dialog', 'true')
    }

    const url = `${auth_endpoint}?${p.toString()}`
    window.location.replace(url)
  }

  extractToken() {

    const p = new HttpParams({
      fromString: window.location.hash
    })
    let access_token = p.get('#access_token')

    if (!access_token) {
      const rawToken = sessionStorage.getItem('token')
      access_token = rawToken && JSON.parse(rawToken)
    } else {
      sessionStorage.setItem('token', JSON.stringify(access_token))
      window.location.hash = ''
    }
    this.token = access_token
  }

  getToken() {
    return this.token
  }
}
