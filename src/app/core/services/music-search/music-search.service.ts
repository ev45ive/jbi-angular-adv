import { EventEmitter, Inject, Injectable } from '@angular/core';
import { CoreModule } from '../../core.module';
import { Album, AlbumsSearchResponse } from '../../model/search';
import { API_SEARCH_URL } from '../../tokens';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { AuthService } from '../auth.service';
import { catchError, concatAll, concatMap, exhaust, map, mergeAll, mergeMap, pluck, startWith, switchAll, switchMap, throttleTime } from 'rxjs/operators'
import { AsyncSubject, BehaviorSubject, concat, EMPTY, merge, of, ReplaySubject, Subject, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
  // providedIn: CoreModule,
})
export class MusicSearchService {
  errors = new Subject<Error>()

  private query = new BehaviorSubject<string>('')
  queryChanges = this.query.asObservable()

  private albums = new BehaviorSubject<Album[]>(mockData as Album[])
  albumsChanges = this.albums.asObservable()

  constructor(
    private http: HttpClient,
    @Inject(API_SEARCH_URL) private api_url: string
  ) {
    // this.albumsChanges.getValue()
    // (window as any).subject = this.albumsChanges

    this.query.pipe(
      map(query => ({
        type: 'album',
        query
      })),
      switchMap(params => this.http.get<AlbumsSearchResponse>(this.api_url, { params }).pipe(
        map(res => res.albums.items),
        catchError((err) => { this.errors.next(err); return EMPTY })
      )),
    )
      .subscribe(this.albums)
  }

  searchAlbums(query: string) {
    this.query.next(query)
  }

  getAlbums() {
    return this.albumsChanges
  }

  getAlbumById(album_id: string) {
    return this.http.get<Album>(`https://api.spotify.com/v1/albums/${album_id}`)
  }
}


export const mockData: Pick<Album, 'id' | 'name' | 'images'>[] = [
  {
    id: '123', name: 'Test',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }
    ]
  },
  {
    id: '234', name: 'Test 234',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }
    ]
  },
  {
    id: '345', name: 'Test 345',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }
    ]
  },
  {
    id: '456', name: 'Test 456',
    images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }
    ]
  },
]