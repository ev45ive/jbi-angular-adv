import { Track } from './search';

export interface Entity {
  id: string;
  name: string;
}
// export interface Track extends Entity {
//   type: 'Track',
//   duration: number
// }

export interface Playlist extends Entity {
  type: 'Playlist',
  public: boolean;
  description: string;
  // tracks: Array<Track>
  tracks?: Track[]
}

// const playlist: Playlist = {
//   type: 'Playlist',
//   id: '123',
//   name: '123',
//   description: '',
//   public: true,
//   tracks: []
// }

// if (playlist.tracks) {
//   playlist.tracks.length
// }
// playlist.tracks && playlist.tracks.length
// playlist.tracks?.length

// const pORt: Playlist | Track = {} as any

// switch (pORt.type) {
//   case 'Playlist':
//     pORt.tracks
//   break
//   case 'Track':
//     pORt.duration
// }


// /*  */

// interface Vector { kind: 'Vector', x: number; y: number, length: number }
// interface Point { kind: 'Point', x: number; y: number }

// let v: Vector = { kind: 'Vector', x: 123, y: 123, length: 123 };
// let p: Point = { kind: 'Point', x: 123, y: 123 };

// // p = v
// // v = p

// let pv: Point | Vector = {} as any

// if (pv.kind == 'Point') {
//   pv.x
// } else {
//   pv.length
// }



