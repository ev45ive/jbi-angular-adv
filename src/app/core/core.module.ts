import { Inject, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';
import { API_SEARCH_URL } from './tokens';
import { MusicSearchService } from './services/music-search/music-search.service';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { AuthConfig } from './services/AuthConfig';
import { AuthInterceptor } from './interceptors/auth.interceptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    // HttpClientXsrfModule.withOptions({
    //   cookieName:'',headerName:''
    // }),
    HttpClientXsrfModule.disable(),
    HttpClientModule,
  ],
  providers: [
    // {
    //   provide: HttpClient,
    //   useClass: MyMuchBetterOverridenHttpClient
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    // {
    //   provide: AuthConfig,
    //   useValue: environment.auth_config
    // }
    // {
    //   provide: MusicSearchService,
    //   useFactory(url: string/* ServiceA, ValueB */) {
    //     return new MusicSearchService(url)
    //   },
    //   deps: [API_SEARCH_URL/* , AService, BToken */]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService,
    //   // deps: [API_SEARCH_URL]
    // },
    // MusicSearchService
  ]
})
export class CoreModule {

  static forRoot(config: { api_url: string }): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        {
          provide: API_SEARCH_URL,
          useValue: config.api_url
        },
      ]
    }
  }


  constructor(private auth: AuthService, @Inject(HTTP_INTERCEPTORS) interceptors: any) {
    // console.log(interceptors)
    this.auth.init()
  }
}
