import { InjectionToken } from '@angular/core';

const tokens = {};
export const API_SEARCH_URL = new InjectionToken<string>("Token for search api");
