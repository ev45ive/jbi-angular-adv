import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, DoBootstrap, Inject, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { MusicSearchModule } from './music-search/music-search.module';
import { environment } from 'src/environments/environment';
import { ROUTES } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    // SuperSpecialWidgetModule,
    CoreModule.forRoot({
      api_url: environment.api_url
    }),
    SharedModule,
    PlaylistsModule,
    MusicSearchModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent, /* SidebarComponent */],
})
export class AppModule /* implements DoBootstrap  */ {

  constructor(appRef: ApplicationRef,
    // @Inject(ROUTES) routes: any
    ) {
    // console.log(routes)
    // appRef.tick()
  }

  // ngDoBootstrap(appRef: ApplicationRef): void {
  //   appRef.bootstrap(AppComponent,'app-root')
  //   appRef.bootstrap(AppComponent,'app-pancakes')
  // }
}
