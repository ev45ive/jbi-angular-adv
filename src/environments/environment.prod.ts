import { AuthConfig } from 'src/app/core/services/AuthConfig';

export const environment = {
  production: true,
  api_url: 'https://api.spotify.com/v1/search',
  auth_config: {
    auth_endpoint: 'https://accounts.spotify.com/authorize',
    client_id: '76f670a88ffc47f097f490142be8e760',
    response_type: 'token',
    redirect_uri: 'http://localhost:4200/',
    state: '',
    scopes: [
      'playlist-read-collaborative',
      'playlist-modify-public',
      'playlist-read-private',
      'playlist-modify-private',
    ],
    show_dialog: true
  } as AuthConfig
};
