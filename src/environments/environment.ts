// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { AuthConfig } from "src/app/core/services/AuthConfig";

export const environment = {
  production: false,
  api_url: 'https://api.spotify.com/v1/search',
  auth_config: {
    auth_endpoint: 'https://accounts.spotify.com/authorize',
    client_id: '76f670a88ffc47f097f490142be8e760',
    response_type: 'token',
    redirect_uri: 'http://localhost:4200/',
    state: '',
    scopes: [
      'playlist-read-collaborative',
      'playlist-modify-public',
      'playlist-read-private',
      'playlist-modify-private',
    ],
    show_dialog: true
  } as AuthConfig
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
