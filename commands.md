# Clone and start
cd ..
git clone https://bitbucket.org/ev45ive/jbi-angular-adv.git
cd jbi-angular-adv
npm i 
npm start

# Installations / Versions
node -v 
v14.5.0

npm -v 
6.14.5

code -v 
1.40.1

git --version
git version 2.23.0.windows.1

npm i -g @angular/cli
ng --version

# VS Code extensions
TSLint
Prettier
Angular Language Service - https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
Angular 10 snippets - https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
switch between files - https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

# Chrome extensions
https://chrome.google.com/webstore/detail/augury/elgalmkoelokbchhkhacckoklkejnhcd?hl=pl

# New Project
cd ..
ng new angular-adv
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss ]

# Generators
ng g m playlists -m app --routing true
ng g c playlists/containers/playlists-view
ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form

# MusicSearch
ng g m music-search -m app --routing true
ng g c music-search/containers/music-search-view
ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card


# Feedback
https://www.linkedin.com/in/mateuszkulesza/

https://www.jbinternational.co.uk/feedback/6538

